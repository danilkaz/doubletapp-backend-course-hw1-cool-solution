from django.urls import path

from app.internal.transport.rest.handlers import InfoAboutMeView

urlpatterns = [path("me/", InfoAboutMeView.as_view()), path("me/<int:telegram_id>/", InfoAboutMeView.as_view())]
