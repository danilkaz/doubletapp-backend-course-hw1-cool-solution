from typing import Any, Optional

from django.core.exceptions import ValidationError
from phonenumber_field.phonenumber import to_python

from app.internal.models.telegram_user import TelegramUser
from config.settings import REGION


def create_telegram_user(telegram_id: int, username: str):
    TelegramUser.objects.get_or_create(telegram_id=telegram_id, defaults={"username": username})


def get_telegram_user_by_parameter(parameter: str, value: Any) -> Optional[TelegramUser]:
    try:
        telegram_user = TelegramUser.objects.get(**{parameter: value})
    except TelegramUser.DoesNotExist:
        return None
    return telegram_user


def save_telegram_user_phone_number(telegram_id: int, phone_number: str):
    telegram_user = get_telegram_user_by_parameter("telegram_id", telegram_id)
    telegram_user.phone_number = phone_number
    telegram_user.save()


def validate_phone_number(phone_number: str):
    if not to_python(phone_number, region=REGION).is_valid():
        raise ValidationError(phone_number)
