from django.db import models
from phonenumber_field.modelfields import PhoneNumberField

from config.settings import REGION


class TelegramUser(models.Model):
    telegram_id = models.IntegerField(primary_key=True, verbose_name="Telegram ID")
    username = models.CharField(max_length=50, unique=True, blank=True, null=True)
    phone_number = PhoneNumberField(region=REGION, blank=True, null=True)

    def __str__(self) -> str:
        return str(self.username)

    class Meta:
        verbose_name = "Telegram user"
        verbose_name_plural = "Telegram users"
