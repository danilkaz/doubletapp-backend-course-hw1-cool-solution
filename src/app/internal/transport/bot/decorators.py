from functools import wraps
from typing import Callable

from telegram import Update
from telegram.ext import CallbackContext

from app.internal.services.user_service import get_telegram_user_by_parameter


def need_phone_number(handler: Callable) -> Callable:
    @wraps(handler)
    def wrapper(update: Update, context: CallbackContext):
        telegram_user = get_telegram_user_by_parameter("telegram_id", update.effective_user.id)
        if not telegram_user.phone_number:
            update.message.reply_text("You don't have a phone number!\n" "Use command /set_phone <phone>")
            return
        return handler(update, context, telegram_user=telegram_user)

    return wrapper
