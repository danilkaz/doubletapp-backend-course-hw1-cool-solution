from typing import Optional

from django.core.exceptions import ValidationError
from telegram import Update
from telegram.ext import CallbackContext

from app.internal.models.telegram_user import TelegramUser
from app.internal.services.user_service import (
    create_telegram_user,
    save_telegram_user_phone_number,
    validate_phone_number,
)
from app.internal.transport.bot.decorators import need_phone_number
from config.settings import REGION


def help_handler(update: Update, context: CallbackContext):
    update.message.reply_text(
        "/start - get started\n"
        "/set_phone <phone> - set phone number\n"
        "/me - get info about me\n"
        "/help - show this message\n"
    )


def start_handler(update: Update, context: CallbackContext):
    user = update.effective_user
    telegram_id = user.id
    username = user.username
    create_telegram_user(telegram_id, username)
    update.message.reply_text("User created!")


def set_phone_handler(update: Update, context: CallbackContext):
    try:
        phone_number = context.args[0]
    except IndexError:
        update.message.reply_text("You didn't enter a phone number!\n" "Use command /set_phone <phone>")
        return
    try:
        validate_phone_number(phone_number)
    except ValidationError:
        answer = "Phone number dialed in wrong format!\n"
        if REGION is not None:
            answer += f"Enter in region format: {REGION}"
        update.message.reply_text(answer)
        return
    save_telegram_user_phone_number(update.effective_user.id, phone_number)
    update.message.reply_text("Phone number saved!")


@need_phone_number
def get_info_about_me_handler(update: Update, context: CallbackContext, telegram_user: Optional[TelegramUser] = None):
    update.message.reply_text(
        f"Telegram ID = `{telegram_user.telegram_id}`\n"
        f"Username = `{telegram_user.username}`\n"
        f"Phone number = `{telegram_user.phone_number}`",
        parse_mode="markdown",
    )
