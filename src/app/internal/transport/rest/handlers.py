from typing import Any, Optional

from django.http import HttpRequest, JsonResponse
from django.views import View

from app.internal.services.user_service import get_telegram_user_by_parameter

ACCESS_DENIED_RESPONSE = JsonResponse({"error": {"message": "access denied"}})

USER_NOT_FOUND_RESPONSE = JsonResponse({"error": {"message": "user is not found"}})

PHONE_NUMBER_NOT_SET_RESPONSE = JsonResponse({"error": {"field": "phone_number", "message": "phone number not set"}})


class InfoAboutMeView(View):
    @staticmethod
    def get(request: HttpRequest, telegram_id: Optional[int] = None) -> JsonResponse:
        if telegram_id is not None:
            if not request.user.is_superuser:
                return ACCESS_DENIED_RESPONSE
            return InfoAboutMeView.get_info_about_telegram_user("telegram_id", telegram_id)
        return InfoAboutMeView.get_info_about_telegram_user("username", request.user.username)

    @staticmethod
    def get_info_about_telegram_user(parameter: str, value: Any) -> JsonResponse:
        telegram_user = get_telegram_user_by_parameter(parameter, value)
        if telegram_user is None:
            return USER_NOT_FOUND_RESPONSE
        if not telegram_user.phone_number:
            return PHONE_NUMBER_NOT_SET_RESPONSE
        return JsonResponse(
            {
                "telegram_id": telegram_user.telegram_id,
                "username": telegram_user.username,
                "phone_number": str(telegram_user.phone_number),
            }
        )
