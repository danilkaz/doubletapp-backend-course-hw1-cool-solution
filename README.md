## Doubletapp Backend Course

# Run Telegram Bot

- Set environment variables similar to .env.example file in .env file

- Run `make bot`

Bot help by command `/help`

# API Endpoint

- available at `/api/me/<id>/` (if you are superuser)

- also `/api/me/` - information about the telegram user with your username will appear
